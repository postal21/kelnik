document.addEventListener("DOMContentLoaded", function () {
  const flatList = document.querySelector(".content__list");
  const headBtns = flatList.querySelectorAll(".content__list-head .item");
  const roomBtns = document.querySelectorAll(".filter .rooms-item");
  const priceRange = document.querySelector(".filter .price-range");
  const squareRange = document.querySelector(".filter .square-range");
  const priceMin = document.querySelector(".filter .price .from span");
  const priceMax = document.querySelector(".filter .price .to span");
  const squareMin = document.querySelector(".filter .square .from span");
  const squareMax = document.querySelector(".filter .square .to span");
  const addMore = document.querySelector(".content__add-more");
  const clearBtn = document.querySelector(".filter .clear");
  const topBtn = document.querySelector(".totop");

  const onePage = 9;

  const filterParam = {
    price: [],
    rooms: [1, 2, 3, 4],
    square: [],
  };

  let filtredRes = {};

  let filterRooms = [];
  let originFullRes = {};
  let showRes = {};
  let minMax = [];
  let flatRes;
  let tmpRes = {};
  let sortFlag = [];

  window.addEventListener("scroll", (e) => {
    console.log(window.pageYOffset);
    window.innerHeight - 150 < window.pageYOffset ? topBtn.classList.add("show") : topBtn.classList.remove("show");
  });

  if (topBtn) {
    topBtn.addEventListener("click", () => {
      document.querySelector(".wrapper").scrollIntoView({
        behavior: "smooth",
        block: "start",
      });
    });
  }

  getData()
    .then((res) => {
      if (Object.keys(res).length > 0) {
        let finishOnLoad = Object.keys(res).length >= onePage ? onePage : Object.keys(res).length;
        originFullRes = res;

        //Объект для хранения результата фильтрации
        filtredRes = res;

        //Создаем фильтра и записываем стартовые значения
        filterParam.price = createFilter(priceRange, filtredRes);
        filterParam.square = createFilter(squareRange, filtredRes);

        //Проверяем скольки комнатные квартиры есть в базе и записиываем стартовое значение
        filterParam.rooms = checkRooms();

        //рендерим карточки
        makeRender(originFullRes, 0, finishOnLoad);

        //Вешаем обработку на кнопки комнат
        checkRoomBtns();

        //Вешаем обработку на кнопку сортировки
        checkHeadBtns();

        //Показываем или нет кнопку подгрузки данных
        checkVisibilityAddMore(Object.keys(res).length);

        //Вешаем события отслеживания на кнопку подгрузки данных и ползунки
        priceRange.noUiSlider.on("end", rangeChange);
        squareRange.noUiSlider.on("end", rangeChange);
        priceRange.noUiSlider.on("slide", rangeChange);
        squareRange.noUiSlider.on("slide", rangeChange);
      }
    })
    .catch((err) => console.log(err));

  //Получаем данные
  async function getData() {
    const response = await fetch("bd.json");

    return response.json();
  }

  //Обработка нажатия на кнопку Сбросить параметры
  if (clearBtn) {
    clearBtn.addEventListener("click", (e) => {
      let th = e.target;
      if (th.dataset.show === "on") {
        filtredRes = originFullRes;
        cleanFlats();
        makeRender(filtredRes, 0, onePage, true, true);

        for (let btn of roomBtns) {
          let num = Number(btn.dataset.rooms);
          filterParam.rooms.includes(num) && (btn.dataset.state = "off");
        }

        th.dataset.show = "off";
        checkVisibilityAddMore(Object.keys(filtredRes).length);
      }
    });
  }

  //Производим рендер страницы
  function makeRender(res, start, finish, update = true, disable = true) {
    //Рендерим карточки квартир
    renderFlats(res, start, finish);

    //устанавливаем число показанных карточек
    flatList.dataset.show = finish;

    //Вешаем события отслеживания на кнопку подгрузки данных
    checkAddMore();

    disable ? checkRooms(res) : checkRooms();

    //Обновляем данные ползунков
    if (update) {
      minMax = calcMinMax(res, "price");
      updateRangeFilter(priceRange, minMax);
      updatePriceMinMax(minMax);
      minMaxS = calcMinMax(res, "square");
      updateRangeFilter(squareRange, minMaxS);
      updateSquareMinMax(minMaxS);
    }
  }

  //Отключаем кнопки отсутствующих квартир
  function checkRooms(res = filtredRes) {
    let avaibleRooms = [];
    let uniqueRooms = [];
    for (let item in res) {
      avaibleRooms.push(res[item]["rooms"]);
    }
    if (avaibleRooms.length > 0) {
      uniqueRooms = avaibleRooms.filter((item, index, array) => {
        return array.indexOf(item) === index;
      });
    }

    for (let btn of roomBtns) {
      let num = Number(btn.dataset.rooms);
      uniqueRooms.includes(num) ? (btn.dataset.exist = "true") : (btn.dataset.exist = "false");
    }

    return uniqueRooms;
  }

  function checkAddMore() {
    let start = flatList.dataset.show;
    let finish = Number(start) + onePage;

    let newFinish = Object.keys(filtredRes).length > finish ? finish : Object.keys(filtredRes).length;

    if (addMore) {
      addMore.addEventListener("click", () => {
        if (Number(newFinish) === Number(Object.keys(filtredRes).length)) {
          addMore.dataset.show = "off";
        }
        cleanFlats();
        makeRender(filtredRes, 0, newFinish);
      });
    }

    if (addMore.dataset.show === "on") {
    }
  }

  //Обработчик кнопок сортировки в Шапке списка
  function checkHeadBtns() {
    let data;

    if (headBtns) {
      for (let btn of headBtns) {
        btn.addEventListener("click", () => {
          let param = btn.dataset.sort;
          let oBy = btn.dataset.orderby;
          data = sortRes(filtredRes, param, oBy);
          btn.dataset.orderby = oBy === "asc" ? "desc" : "asc";

          for (let item of headBtns) {
            item.dataset.active = "false";
          }
          btn.dataset.active = "true";

          sortFlag = [param, oBy];
        });
      }
    }
  }

  //Обработчик кнопки количества комнат в фильтре
  function checkRoomBtns() {
    let rooms;
    if (roomBtns.length > 0) {
      for (let btn of roomBtns) {
        btn.addEventListener("click", (e) => {
          if (e.target.dataset.exist === "true" && e.target.dataset.state === "off") {
            btn.dataset.state = btn.dataset.state === "on" ? "off" : "on";
            rooms = document.querySelectorAll('.rooms-item[data-state*="on"]');
            if (rooms.length > 0) {
              filterRooms = [];
              for (let room of rooms) {
                filterRooms.push(room.dataset.rooms);
              }
              clearBtn.dataset.show === "off" && (clearBtn.dataset.show = "on");
            } else {
              filterRooms = [];
            }
            filterRes(filtredRes);
            minMax = calcMinMax(filtredRes, "price");
            updateRangeFilter(priceRange, minMax);
            minMaxS = calcMinMax(filtredRes, "square");
            updateRangeFilter(squareRange, minMaxS);
          }
        });
      }
    }
  }

  function filterRes(res) {
    let result = {};
    let i = 0;
    let finish;

    if (filterRooms.length > 0) {
      for (let item in res) {
        for (let num of filterRooms) {
          if (res[item].rooms === Number(num)) {
            result[i] = res[item];
            i++;
          }
        }
      }
      filtredRes = result;
    } else {
      filtredRes = originFullRes;
    }

    cleanFlats();

    if (Object.keys(filtredRes).length > onePage) {
      finish = 9;
    } else {
      finish = Object.keys(filtredRes).length;
    }

    checkVisibilityAddMore(Object.keys(filtredRes).length);

    makeRender(filtredRes, 0, finish, true, true);
  }

  //Сортировка и рендер нового списка
  function sortRes(res, param, oBy) {
    let sorted = {};
    let tmpSort;
    let finish = flatList.dataset.show;
    if (param !== undefined) {
      if (oBy === "desc") {
        tmpSort = Object.keys(res).sort((a, b) => {
          return res[b][param] - res[a][param];
        });
      } else {
        tmpSort = Object.keys(res).sort((a, b) => {
          return res[a][param] - res[b][param];
        });
      }

      tmpSort.forEach((v, i = 0) => {
        sorted[i] = res[v];
        i++;
      });
      filtredRes = sorted;

      cleanFlats();
      makeRender(filtredRes, 0, finish, false);
    }
  }

  //Обновление ползунков
  function updateRangeFilter(place, minMax) {
    let max = Number(minMax[0]) === Number(minMax[1]) ? Number(minMax[1]) + 0.1 : Number(minMax[1]);
    place.noUiSlider.updateOptions({
      start: [Number(minMax[0]), Number(minMax[1])],
      range: {
        min: Number(minMax[0]),
        max: max,
      },
    });
  }

  //Функция при изменение ползунка фильтра
  function rangeChange(values, handle, unencoded, tap, positions, noUiSlider) {
    let places;
    let range;
    let tmpMinMax = [];
    if (noUiSlider.target.classList.contains("price-range")) {
      places = priceRange;
      range = "price";
    } else if (noUiSlider.target.classList.contains("square-range")) {
      places = squareRange;
      range = "square";
    }

    flatRes = calcRange(filtredRes, range, Number(values[0]), Number(values[1]));
    tmpMinMax[0] = calcMinMax(flatRes, "price");
    tmpMinMax[1] = calcMinMax(flatRes, "square");
    if (range == "price") {
      squareRange.noUiSlider.updateOptions({
        start: [Number(tmpMinMax[1][0]), Number(tmpMinMax[1][1])],
      });
    } else if (range == "square") {
      priceRange.noUiSlider.updateOptions({
        start: [Number(tmpMinMax[0][0]), Number(tmpMinMax[0][1])],
      });
    }

    checkVisibilityAddMore(Object.keys(flatRes).length);

    updatePriceMinMax(tmpMinMax[0]);
    updateSquareMinMax(tmpMinMax[1]);

    clearBtn.dataset.show === "off" && (clearBtn.dataset.show = "on");

    cleanFlats();

    makeRender(flatRes, 0, onePage, false, true);
  }

  //Функция создания фильтра
  function createFilter(place, res) {
    let step;
    if (place.classList.contains("price-range")) {
      minMax = calcMinMax(res, "price");
      updatePriceMinMax(minMax);
      step = 10000;
    } else if (place.classList.contains("square-range")) {
      minMax = calcMinMax(res, "square");
      updateSquareMinMax(minMax);
      step = 0.1;
    }

    noUiSlider.create(place, {
      start: [0, minMax[1]],
      connect: true,
      step: step,
      range: {
        min: Number(minMax[0]),
        max: Number(minMax[1]),
      },
    });

    return minMax;
  }

  //Функция расчет минимальной и максимальной цены
  function calcMinMax(res, place) {
    let i = 0;
    let result = [];
    for (let item in res) {
      if (i === 0) {
        result[0] = res[item][place];
        result[1] = res[item][place];
      }
      result[0] >= res[item][place] && (result[0] = res[item][place]);
      result[1] <= res[item][place] && (result[1] = res[item][place]);
      i++;
    }

    return result;
  }

  //Функция для расчет данных из ползунков
  function calcRange(res, place, start, end) {
    let i = 0;
    let result = {};
    for (let item in res) {
      if (res[item][place] >= start && res[item][place] <= end) {
        result[i] = res[item];
        i++;
      }
    }

    return result;
  }

  //Обновляем минимум и максимум у цены и площади
  function updatePriceMinMax(minMax) {
    priceMin.innerText = minMax[0].toLocaleString("ru-RU");
    priceMax.innerText = minMax[1].toLocaleString("ru-RU");
  }
  function updateSquareMinMax(minMax) {
    squareMin.innerText = Number(minMax[0]);
    squareMax.innerText = Number(minMax[1]);
  }

  //Прорисовка списка квартир
  function renderFlats(flatRes, start, finish) {
    let fin = Object.keys(flatRes).length > finish ? Number(finish) + 1 : Object.keys(flatRes).length;
    for (let i = start; i < fin; i++) {
      let flat = createFlatItem(flatRes[i]);
      flatList.appendChild(flat);
      showRes[i] = flatRes[i];
    }
  }

  //Создаем строку с квартирой
  function createFlatItem(elem) {
    let parent = createElement("div", ["content__list-elem", "content__list-flat"]);
    for (let item in elem) {
      if (item !== "hFloor" && item !== "rooms") {
        let div = createElement("div", ["item", item]);
        let txt;
        if (item === "plan") {
          txt = `<img src="${elem[item][0]}" alt="${elem[item][1]}" />`;
        } else if (item === "floor") {
          txt = `${elem[item]} <span>из ${elem["hFloor"]}</span>`;
        } else if (item === "price") {
          txt = elem[item].toLocaleString("ru-RU");
        } else {
          txt = elem[item];
        }
        div.innerHTML = txt;
        parent.appendChild(div);
      }
    }

    return parent;
  }

  //Создаем HTML элемент
  function createElement(type, className) {
    let elem = document.createElement(type);
    for (let item of className) {
      elem.classList.add(item);
    }

    return elem;
  }

  //Удаление всех квартир
  function cleanFlats() {
    let flats = flatList.querySelectorAll(".content__list-flat");
    for (let flat of flats) {
      flat.remove();
    }
  }

  //Проверка для отображения или скрытия кнопки Загрузить еще
  function checkVisibilityAddMore(res) {
    addMore.dataset.show = res < onePage ? "off" : "on";
  }
});
